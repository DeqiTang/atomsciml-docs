# AtomSciML Documentations 
Documentaions of AtomSciML, a software aimed to provide an Machine Learning envrionment for Scientific Researches that involve atom.

# Philosophy
Application of Machine Learning in Atom Science can be versatile. Let's take the Ab Initio computation
for example. The involving of Machine Learning in different phase of problem solving can result in totally
different application. It can be classified to Machine Learning DFT, Machine Learning Force Fields and 
Machine Learning Property Predictor.

## License
AtomSciML Documentaion is released under the MIT License. The terms of the license are as follows:
```
MIT License

Copyright (c) 2022 DeqiTang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

