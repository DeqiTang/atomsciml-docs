.. Atomsciml Documentaion documentation master file, created by
   sphinx-quickstart on Mon Jan 10 13:10:28 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Documentaion for Atomsciml
==================================================
Atomsciml is designed to accommodate researchers with a Machine Learning environment for Scientific Researches where atom is involved.

Philosopy
==================
Application of Machine Learning in Atom Science can be versatile. Let's take the Ab Initio computation
for example. The involving of Machine Learning in different phase of problem solving can result in totally
different application. It can be classified to Machine Learning DFT, Machine Learning Force Fields and 
Machine Learning Property Predictor, etc.


Future
==================
The project is under continuous development! We will hug the future!


.. toctree::
   :maxdepth: 1 
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
